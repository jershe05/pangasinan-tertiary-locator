const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
let aNavsSass = [
    'resources/sass/navs/_global.scss',
    'resources/sass/navs/_sidebar.scss',
    'resources/sass/navs/_topbar.scss',

];
let aAdminJS = [
    'resources/js/Controllers/Category/CategoryController.js'
];

mix.copyDirectory('resources/css/webfonts/', 'public/css/webfonts');
mix.copyDirectory('resources/img/', 'public/img');

mix.js('resources/js/app.js', 'public/js');

mix.js('resources/js/Controllers/Account/UserRegistrationController.js', 'public/js');
mix.js('resources/js/Controllers/Account/LoginController.js', 'public/js');


mix.js(aAdminJS, 'public/js/admin.js');

mix.js('resources/js/loadingModal.js', 'public/js');
mix.js('resources/js/jquery.easing.js', 'public/js/jquery-easing.js');

mix.sass('resources/sass/app.scss', 'public/css');
mix.styles('resources/css/all.css', 'public/css/all.css');
mix.styles('resources/css/loadingModal.css', 'public/css/loadingModal.css');

