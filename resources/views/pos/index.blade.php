@extends('layout.default')
@section('title')
    Point of Sale
@endsection

@push('styles')


@endpush

<div id="content">
    @component('layout.nav')
    @endcomponent
            <!-- Begin Page Content -->
            <div class="container-fluid">
                <div class="row">
                  @component('product.section')
                      @endcomponent

                  @component('pos.cart')
                  @endcomponent

                </div>
            </div>
            <!-- /.container-fluid -->
</div>
        <!-- End of Main Content -->


<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

@component('layout.logout')
    @endcomponent

@component('pos.transaction-details')
    @endcomponent

@component('pos.order-details')
    @endcomponent

<div class="text-center">
    <a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalContactForm">Launch
        Modal Contact Form</a>
</div>

<div class="text-center">
    <a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalOrderForm">Launch
        Modal Contact Form</a>
</div>
@push('script')

    @endpush
