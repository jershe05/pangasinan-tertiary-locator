
<!-- modal order -->
<div class="modal fade" id="modalOrderForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">ORDER SETTINGS</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="input-group border mb-1">
                    <div class="input-group-append p-3  ">
                        <i class="fas fa-barcode fa-2x" ></i>
                    </div>
                    <div class="p-3">0000101010</div>
                </div>

                <div class="input-group border mb-1">
                    <div class="input-group-append p-3  ">
                        <i class="fas fa-fingerprint fa-2x" ></i>
                    </div>
                    <div class="p-3">Party Balloons</div>
                </div>

                <div class="input-group border mb-1">
                    <div class="input-group-append p-3">
                        <i class="fas fa-cubes fa-2x" ></i>
                    </div>
                    <div class="dropdown mb-1">
                        <button class="btn btn-primary dropdown-toggle p-3 mt-2" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Select Package
                        </button>
                        <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Per Package</a>
                            <a class="dropdown-item" href="#">Per Dozen</a>
                            <a class="dropdown-item" href="#">Per Item</a>
                        </div>
                    </div>

                    <div class="input-group-append ml-2 border">
                        <button class="btn btn-secondary m-1" type="button">
                            <i class="fas fa-list-ol"></i>
                        </button>
                        <input type="text" class="border-0 w-90 ml-2 input-focus_ p-3" placeholder="quantity..." aria-label="Search" aria-describedby="basic-addon2">
                    </div>
                </div>

                <!-- Color Selection -->
                <div class="input-group border mb-1">
                    <div class="input-group-append p-3">
                        <i class="fas fa-palette fa-2x" ></i>
                    </div>

                        <div class="dropdown mb-1">
                            <button class="btn btn-primary dropdown-toggle p-3 mt-2" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Select Color
                            </button>
                            <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">Per Package</a>
                                <a class="dropdown-item" href="#">Per Dozen</a>
                                <a class="dropdown-item" href="#">Per Item</a>
                            </div>
                        </div>

                        <div class="input-group-append ml-2 border">
                            <button class="btn btn-secondary m-1" type="button">
                                <i class="fas fa-list-ol"></i>
                            </button>
                            <input type="text" class="border-0 w-90 ml-2 input-focus_ p-3" placeholder="quantity..." aria-label="Search" aria-describedby="basic-addon2">
                            <button class="btn btn-danger" type="button">
                                <i class="fas fa-plus fa-2x p-1" ></i>
                            </button>
                        </div>
                </div>

                <!-- Color List -->
                <div class="input-group border mb-1">
                    <div class="p-1">
                         <span class="mr-2">
                            <i class="fas fa-circle" ></i>
                        </span>
                        <span class="mr-2">
                           2
                            </span>
                    </div>
                </div>

                <!-- Design Selection -->
                <div class="input-group border mb-1">
                    <div class="input-group-append p-3">
                        <i class="fas fa-splotch fa-2x" ></i>
                    </div>

                    <div class="dropdown mb-1">
                        <button class="btn btn-primary dropdown-toggle p-3 mt-2" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Select Design
                        </button>
                        <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Per Package</a>
                            <a class="dropdown-item" href="#">Per Dozen</a>
                            <a class="dropdown-item" href="#">Per Item</a>
                        </div>
                    </div>

                    <div class="input-group-append ml-2 border">
                        <button class="btn btn-secondary m-1" type="button">
                            <i class="fas fa-list-ol"></i>
                        </button>
                        <input type="text" class="border-0 w-90 ml-2 input-focus_ p-3" placeholder="quantity..." aria-label="Search" aria-describedby="basic-addon2">
                        <button class="btn btn-danger" type="button">
                            <i class="fas fa-plus fa-2x p-1" ></i>
                        </button>
                    </div>
                </div>

                <!-- Designs Selected -->
                <div class="input-group border mb-1">
                    <div class="card py-3 border-bottom-info p-2">Doraemon</div>
                </div>
                <hr class="topbar-divider" />
                <div class="input-group mb-1 p-2 font-weight-bold">
                   <span>Pick-up Date and Time</span>
                </div>
                <div class="input-group border mb-1">
                    <div class="input-group-append p-3">
                        <i class="fas fa-calendar-alt fa-2x" ></i>
                    </div>
                    <input type="datetime-local" class="border-0 w-90 ml-2 input-focus_ text-danger font-weight-bold" aria-label="Search" aria-describedby="basic-addon2">
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button class="btn btn-unique btn-danger w-100" data-dismiss="modal">SAVE <i class="fas fa-save ml-1"></i></button>
            </div>
        </div>
    </div>
</div>
