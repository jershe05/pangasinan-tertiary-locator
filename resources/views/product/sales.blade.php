<div class="col-xl-5 col-lg-5">
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Product Details and Sales <i class="fas fa-award"></i></h6>
            <div class="dropdown no-arrow">
                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                    <div class="dropdown-header">Product Settings</div>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modalAddProduct"><i class="fas fa-edit"></i> Update</a>
                    <a class="dropdown-item" href="#"><i class="fas fa-minus-square"></i> Delete</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#"><i class="fas fa-copy"></i> Copy</a>
                </div>
            </div>
        </div>
        <div class="modal-content">
           @component('layout.carousel')
               @endcomponent
                <div class="modal-body mx-3">
                    <div class="input-group border mb-1">
                        <div class="input-group-append p-3  ">
                            <i class="fas fa-barcode fa-2x" ></i>
                        </div>
                        <div class="p-3">0000101010</div>
                    </div>

                    <div class="input-group border mb-1">
                        <div class="input-group-append p-3  ">
                            <i class="fas fa-fingerprint fa-2x" ></i>
                        </div>
                        <div class="p-3">Party Balloons</div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Color</th>
                                <th>Size</th>
                                <th>Qty</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <div class="card py-3 border-bottom-red p-2 pl-2">Red</div>
                                </td>
                                <td>12</td>
                                <td>2</td>
                                <td>
                                    <i class="fas fa-check-double fa-2x color-green"></i>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="card py-3 border-bottom-green p-2 pl-2">Green</div>
                                </td>
                                <td>10</td>
                                <td>0</td>
                                <td>
                                    <i class="fas fa-times fa-2x color-red"></i>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr class="topbar-divider" />
                    <div class="input-group mb-1 p-2 font-weight-bold">
                        <span>Date Last Updated</span>
                    </div>
                    <div class="input-group border mb-1">
                        <div class="input-group-append p-3">
                            <i class="fas fa-calendar-alt fa-2x" ></i>
                        </div>

                    </div>
                </div>
            </div>
    </div>
</div>
