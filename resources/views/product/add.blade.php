
<!-- modal order -->
<div class="modal fade" id="modalAddProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">New Product</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="input-group border mb-1">
                    <div class="input-group-append p-3  ">
                        <i class="fas fa-barcode fa-2x" ></i>
                    </div>
                    <input type="text" class="border-0 w-90 ml-2 input-focus_ p-3" placeholder="barcode..." aria-label="Search" aria-describedby="basic-addon2">
                </div>

                <div class="input-group border mb-1">
                    <div class="input-group-append p-3  ">
                        <i class="fas fa-fingerprint fa-2x" ></i>
                    </div>
                    <input type="text" class="border-0 w-90 ml-2 input-focus_ p-3" placeholder="product name..." aria-label="Search" aria-describedby="basic-addon2">
                </div>

                <div class="input-group border mb-1">
                    <div class="input-group-append p-3 pb-3 h-100">
                        <i class="fas fa-window-restore fa-2x" ></i>
                    </div>
                    <div class="dropdown mb-1 w-90">
                        <button class="btn btn-primary dropdown-toggle p-3 mt-2" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Select Category
                        </button>
                        <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton">
                            <span class="dropdown-item clickable"><i class="fas fa-square"></i> Per Item</span>
                            <span class="dropdown-item clickable"><i class="fas fa-list-ol"></i> Per Dozen</span>
                            <span class="dropdown-item clickable"><i class="fas fa-box-open"></i> Per Pack</span>
                        </div>
                    </div>

                </div>

                <div class="input-group border mb-1">
                    <div class="input-group-append p-3 pb-3 h-100">
                        <i class="fas fa-cubes fa-2x" ></i>
                    </div>
                    <div class="dropdown mb-1 w-90">
                        <button class="btn btn-primary dropdown-toggle p-3 mt-2" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Select Packs
                        </button>
                        <button class="btn btn-danger p-2 mt-2" type="button">
                            <i class="fas fa-plus fa-2x p-1" ></i>
                        </button>
                        <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton">
                            <span class="dropdown-item clickable"><i class="fas fa-square"></i> Per Item</span>
                            <span class="dropdown-item clickable"><i class="fas fa-list-ol"></i> Per Dozen</span>
                            <span class="dropdown-item clickable"><i class="fas fa-box-open"></i> Per Pack</span>
                        </div>
                    </div>
                    <div class="input-group-append p-3 pb-3 h-100 d-none">
                        <i class="fas fa-copy fa-2x" ></i>
                    </div>
                    <div class="input-group-append ml-1 w-50 d-none">
                        <input type="text" class="border-0 w-100 ml-2 input-focus_ p-3" placeholder="No. of Items/Pack..." aria-label="Search" aria-describedby="basic-addon2">
                    </div>
                </div>

                <div class="input-group border mb-1">
                    <div class="p-3 border-bottom-info">
                        <i class="fas fa-square"></i> <label>Per Item</label>
                        <button type="button" class="close pl-2" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="p-3 border-bottom-info">
                        <i class="fas fa-list-ol"></i> <label>Per Dozen</label>
                        <button type="button" class="close pl-2" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="p-3 border-bottom-info">
                        <i class="fas fa-box-open"></i> <label>Per Pack</label>
                        <button type="button" class="close pl-2" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>

                <!-- Color Selection -->
                <div class="input-group border mb-1">
                    <div class="input-group-append p-3">
                        <i class="fas fa-palette fa-2x" ></i>
                    </div>

                    <div class="dropdown mb-1">
                        <button class="btn btn-primary dropdown-toggle p-3 mt-2" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Select Color
                        </button>
                        <button class="btn btn-danger p-2 mt-2" type="button">
                            <i class="fas fa-plus fa-2x p-1" ></i>
                        </button>
                        <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item color-red" href="#">Red</a>
                            <a class="dropdown-item color-blue" href="#">Blue</a>
                            <a class="dropdown-item color-green" href="#">Green</a>
                        </div>
                    </div>
                </div>

                <!-- Color List -->
                <div class="input-group border mb-1">
                    <div class="p-3 border-bottom-info align-items-center">
                        <i class="fas fa-circle color-green"></i>
                        <button type="button" class="close pl-2" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="p-3 border-bottom-info align-items-center">
                        <i class="fas fa-circle color-red"></i>
                        <button type="button" class="close pl-2" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="p-3 border-bottom-info align-items-center">
                        <i class="fas fa-circle color-pink"></i>
                        <button type="button" class="close pl-2" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="p-3 border-bottom-info align-items-center">
                        <i class="fas fa-circle color-blue"></i>
                        <button type="button" class="close pl-2" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="p-3 border-bottom-info align-items-center">
                        <i class="fas fa-circle color-white"></i>
                        <button type="button" class="close pl-2" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                </div>

                <!-- Design Selection -->
                <div class="input-group border mb-1">
                    <div class="input-group-append p-3">
                        <i class="fas fa-splotch fa-2x" ></i>
                    </div>

                    <div class="dropdown mb-1">
                        <button class="btn btn-primary dropdown-toggle p-3 mt-2" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Select Design
                        </button>
                        <button class="btn btn-danger p-2 mt-2" type="button">
                            <i class="fas fa-plus fa-2x p-1" ></i>
                        </button>
                        <div class="dropdown-menu animated--fade-in" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Frozen</a>
                            <a class="dropdown-item" href="#">Doraemon</a>
                            <a class="dropdown-item" href="#">Batman</a>
                        </div>
                    </div>
                </div>

                <!-- Designs Selected -->
                <div class="input-group border mb-1">
                    <div class="card py-3 border-bottom-info p-2">Doraemon</div>
                </div>

                <div class="input-group border mb-1">
                    <div class="input-group-append p-3">
                        <i class="fas fa-splotch fa-2x" ></i>
                    </div>
                    <textarea type="text" class="border-0 w-90 ml-2 input-focus_ p-3" placeholder="Description..." aria-label="Search" aria-describedby="basic-addon2"></textarea>
                </div>

            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button class="btn btn-unique btn-danger w-100" data-dismiss="modal"> <i class="fas fa-plus ml-1"></i> ADD PRODUCT</button>
            </div>
        </div>
    </div>
</div>
