
<!-- modal order -->
<div class="modal fade" id="modalAddStock" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Add Product Stock</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="input-group border mb-1">
                    <div class="input-group-append p-3  ">
                        <i class="fas fa-barcode fa-2x" ></i>
                    </div>
                    <input type="text" class="border-0 w-90 ml-2 input-focus_ p-3" placeholder="barcode..." aria-label="Search" aria-describedby="basic-addon2">
                </div>
                <div class="input-group border mb-1">
                    <div class="p-3 align-items-center text-danger">
                       Not Found...
                    </div>
                </div>
                <div class="input-group border mb-1">
                    <div class="input-group-append p-3  ">
                        <i class="fas fa-signature fa-2x" ></i>
                    </div>
                    <input type="text" class="border-0 w-90 ml-2 input-focus_ p-3" placeholder="product name..." aria-label="Search" aria-describedby="basic-addon2">
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Color</th>
                            <th>Size</th>
                            <th>Design</th>
                            <th>Qty</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <div class="card py-3 border-bottom-red p-2 pl-2">Red</div>
                            </td>
                            <td>12</td>
                            <td>Frozen</td>
                            <td>
                               2
                            </td>
                            <td>
                                <button class="btn btn-primary p-3"> <i class="fas fa-plus-circle ml-1"></i> </button>
                                <button class="btn btn-info p-3"> <i class="fas fa-edit ml-1"></i> </button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="card py-3 border-bottom-green p-2 pl-2">Green</div>
                            </td>
                            <td>10</td>
                            <td>Mickey Mouse</td>
                            <td>0</td>
                            <td>
                                <button class="btn btn-primary p-3"> <i class="fas fa-plus-circle ml-1"></i> </button>
                                <button class="btn btn-info p-3"> <i class="fas fa-edit ml-1"></i> </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
