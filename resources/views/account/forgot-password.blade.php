@extends('layout.default')
@section('title')
    Forgot Password
@endsection
@section('center')
    align-content-center
@endsection
@section('content')
    @component('account.forms.forgot-password-form')
    @endcomponent
@endsection
@push('scripts')
    <script src="{{ asset('UserRegistration.jsn.js') }}"></script>
@endpush

