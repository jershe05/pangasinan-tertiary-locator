
<!-- modal order -->
<div class="modal fade" id="modalAddCategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">New Category</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="input-group parent-border mb-1" id="category_div">
                    <div class="input-group-append p-3  ">
                        <i class="fas fa-window-restore fa-2x" ></i>
                    </div>
                    <input type="text" id="category_name" class="w-90 ml-2 input-focus_ p-3 border-0" placeholder="Name..." aria-label="Search" aria-describedby="basic-addon2">
                </div>

                <div class="input-group parent-border mb-1">
                    <div class="input-group-append p-3">
                        <i class="fas fa-stream fa-2x" ></i>
                    </div>
                    <textarea type="text" id="category_description" class="border-0 w-90 ml-2 input-focus_ p-3" placeholder="Description..." aria-label="Search" aria-describedby="basic-addon2"></textarea>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button class="btn btn-unique btn-danger w-100" id="category_add_btn"> <i class="fas fa-plus ml-1"></i> ADD CATEGORY</button>
            </div>
        </div>
    </div>
</div>
