
<!-- modal order -->
<div class="modal fade" id="modalCategoryList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static"
     aria-hidden="true">
    <div class="modal-dialog list-modal" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Category List</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="modal-footer d-flex">
                    <button class="btn btn-unique btn-danger" id="category_add_btn" data-dismiss="modal"> <i class="fas fa-plus ml-1"></i> ADD CATEGORY</button>
                </div>
                <div class="input-group parent-border mb-1" id="category_div">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="checkBox-all" checked>
                                        <label class="custom-control-label" for="checkBox-all">All</label>
                                    </div>
                                </th>
                                <th>Category Name</th>
                                <th>Description</th>
                                <th>Update/Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="row1" checked>
                                        <label class="custom-control-label" for="row1"></label>
                                    </div>
                                </td>
                                <td>12</td>
                                <td class="w-50">2</td>
                                <td class="w-25">
                                    <div class="btn btn-primary btn-user btn-block w-40 d-inline" id="submit_btn">
                                        <i class="fas fa-pen"></i>
                                        <span class="span-button">Update</span>
                                    </div>
                                    <div class="btn btn-danger btn-user btn-block w-40 d-inline ml-1" id="submit_btn">
                                        <i class="fas fa-minus"></i>
                                        <span class="span-button">Delete</span>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
