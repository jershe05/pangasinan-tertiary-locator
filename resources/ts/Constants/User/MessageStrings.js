const MESSAGE_STRINGS = {
    ERROR_FIRST_NAME      : 'Please fill-out the name field',
    ERROR_LAST_NAME       : 'Please fill-out the Last Name field',
    ERROR_EMAIL_EMPTY     : 'Please fill-out the Email field',
    ERROR_EMAIL_INVALID   : 'Please Enter Valid E-mail',
    ERROR_PASSWORD        : 'Please fill-out the Password field',
    ERROR_REPEAT_PASSWORD : 'Password did not match',
    REGISTRATION_SUCCESS  : 'Registration Successful'
};

export default MESSAGE_STRINGS;
