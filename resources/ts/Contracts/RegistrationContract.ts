interface RegistrationContract {
    validate() : void;
    confirm() : Promise<void>;
}
export default RegistrationContract;
