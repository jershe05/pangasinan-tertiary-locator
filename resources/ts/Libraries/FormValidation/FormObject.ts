import FormObjectContract from "./FormValidationContracts/FormObjectContract";
import ObjectValidationContract from "./FormValidationContracts/ValidationContract";

class FormObject implements FormObjectContract
{
    private sErrorMessage        : string;
    private bRequired            : boolean;
    private sEmptyErrorMessage   : string;
    private sInvalidErrorMessage : string;
    readonly oObject             : any;
    readonly oObjectValidation   : ObjectValidationContract;

    constructor(oObject : any, oObjectValidation : ObjectValidationContract)
    {
        this.oObject = oObject;
        this.oObjectValidation = oObjectValidation;
    }

    setOptional() : FormObjectContract
    {
        this.bRequired = false;
        return this;
    }

    setErrorMessage(sEmptyErrorMessage, sInvalidErrorMessage) : FormObjectContract {
        this.sEmptyErrorMessage = sEmptyErrorMessage;
        this.sInvalidErrorMessage = sInvalidErrorMessage;
        return this;
    }

    setMessageAndCheckIfEmpty() : boolean {
        if (this.oObject.val().length < 1) {
            this.sErrorMessage = this.sEmptyErrorMessage;
            return true;
        }
        this.sErrorMessage = this.sInvalidErrorMessage;
        return false;
    }

    getErrorMessage() : string {
        return this.sErrorMessage;
    }

    getElement() : any {
        return this.oObject;
    }

    IfValid() : boolean {
        return this.oObjectValidation.sanitize(this.oObject)
            .checkRule()
            .getResult();
    }
}
export default FormObject;
