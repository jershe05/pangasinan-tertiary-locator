interface ObjectValidationContract
{
    sanitize(oObject : any) : ObjectValidationContract;
    checkRule() : ObjectValidationContract;
    getResult() : boolean;
}
export default ObjectValidationContract;
