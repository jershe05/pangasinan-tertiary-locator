import RegistrationContract from "../../Contracts/RegistrationContract"
import ConfirmPopUpContract from "../../Libraries/Confirmation/ConfirmPopUpContract";
import ConfirmPopUp from "../../Libraries/Confirmation/ConfirmPopUp";
import MESSAGE_STRINGS from "../../Constants/Category/MessageStrings";

class AddCategory implements RegistrationContract
{
    private bResult: boolean;
    protected oConfirmPopUp: ConfirmPopUpContract;
    constructor(oConfirmPopUp : ConfirmPopUpContract){
        oConfirmPopUp = new ConfirmPopUp(MESSAGE_STRINGS.CONFIRM_CONTINUE, MESSAGE_STRINGS.YES_LABEL);
        this.oConfirmPopUp = oConfirmPopUp;
    }

    validate(): void {
        let oValidation = this.oFormValidation.checkErrors();
        this.bResult = oValidation.bResult;
        this.sMessage = oValidation.sMessage;
    }

   async confirm() : Promise<void> {
        let oConfirmationResult : boolean;
        if (this.bResult === true) {
            this.bResult = await this.oConfirmPopUp.showConfirmPopUp();
        }
    }

}
