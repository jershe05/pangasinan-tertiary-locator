const USER_OBJECTS = {
    FNAME            : $('#firstName'),
    LNAME            : $('#lastName'),
    EMAIL            : $('#inputEmail'),
    PASSWORD         : $('#inputPassword'),
    CONFIRM_PASSWORD : $('#repeatPassword'),
    SUBMIT_BTN       : $('#add_user_submit_btn')
};

export default USER_OBJECTS;
