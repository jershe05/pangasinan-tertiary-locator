import Swal from "sweetalert2";

class toast
{
    constructor(bResult, sMessage, sUrl = '')
    {
        this.bResult = bResult;
        this.sMessage = sMessage;
        this.sUrl = sUrl;
    }

    showToast()
    {
        if (this.bResult === false) {
            this.showErrorToast();
            return;
        }
        this.showSuccessToast();
    }

    showErrorToast()
    {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            html: this.sMessage,
            footer: '<a href>Why do I have this issue?</a>'
        });
    }

    showSuccessToast()
    {
        let sUrl = this.sUrl;
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: this.sMessage,
            showConfirmButton: false,
            timer: 1500
        }).then(function() {
            if (sUrl.trim()) {
                window.location = sUrl;
            }
        });
    }
}
export default toast;
