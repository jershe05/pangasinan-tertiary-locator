class email
{
    constructor(oEmail) {
        this.oEmail = oEmail;
        this.sErrorMessage = '';
        this.bRequired = true;
    }

    setOptional()
    {
        this.bRequired = false;
        return this;
    }

    setErrorMessage(sEmptyErrorMessage, sInvalidErrorMessage) {
        this.sEmptyErrorMessage = sEmptyErrorMessage;
        this.sInvalidErrorMessage = sInvalidErrorMessage;
        return this;
    }

    setMessageAndCheckIfEmpty() {
        if (this.oEmail.val().length < 1) {
            this.sErrorMessage = this.sEmptyErrorMessage;
            return true;
        }
        this.sErrorMessage = this.sInvalidErrorMessage;
        return false;
    }

    getErrorMessage() {
        return this.sErrorMessage;
    }

    getElement() {
        return this.oEmail;
    }

   IfValid() {
        this.oEmail.val($.trim(this.oEmail.val()));
       if (this.setMessageAndCheckIfEmpty() === true && this.bRequired === false) {
           return true;
       }
        let sValidator = /\S+@\S+\.\S+/;
        return sValidator.test(String(this.oEmail.val()).toLowerCase());
    }
}

export default email;
