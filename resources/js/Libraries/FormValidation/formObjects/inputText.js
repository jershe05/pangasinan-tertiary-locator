class inputText
{
    constructor(oInputText) {
        this.oInputText = oInputText;
        this.bRequired = true;
        this.sErrorMessage = '';
    }

    setOptional()
    {
        this.bRequired = false;
        return this;
    }

    setErrorMessage(sErrorMessage) {
        this.sErrorMessage = sErrorMessage;
        return this;
    }

    getErrorMessage() {
        return this.sErrorMessage;
    }

    IfValid() {
        this.oInputText.val($.trim(this.oInputText.val()));
        if (this.bRequired === true) {
            return this.oInputText.val().length > 1;
        }
        return true;
    }

    getElement() {
        return this.oInputText;
    }

}

export default inputText;
