import MESSAGE_STRINGS from "../../Constants/Login/MessageStrings";
import Toast from "../../Libraries/Toasts/Toast";
import setErrorMessages from "../../Libraries/ErrorMessages/setErrorMessages";
import GLOBAL_STRINGS from "../../Constants/Global/GlobalStrings";

class BlLogin
{
    constructor(oModel, oFormValidation, oUserFormElements)
    {
        this.oModel = oModel;
        this.oFormValidation = oFormValidation;
        this.oUserFormElements = oUserFormElements;
    }

    async login()
    {
        let oValidation = this.oFormValidation.checkErrors();
        if (oValidation.bResult === true) {
            let oResult = await this.oModel.authenticate();
            this.showMessage(oResult);
            return;
        }
        this.displayMessage(oValidation.bResult, oValidation.sMessage, '');
    }

    showMessage(oResult)
    {
        let sMessage = MESSAGE_STRINGS.LOGIN_SUCCESS;
        let oErrorMessage = new setErrorMessages(oResult[GLOBAL_STRINGS.RESULT], this.oUserFormElements);

        sMessage = oErrorMessage.setErrorMessages(sMessage, oResult[GLOBAL_STRINGS.B_RESULT]);
        this.displayMessage(oResult[GLOBAL_STRINGS.B_RESULT], sMessage, oResult['url'])
    }

    displayMessage(bResult, sMessage, sUrl) {
        let oToast = new Toast(bResult, sMessage, sUrl);
        oToast.showToast();
    }

}
export default BlLogin;
