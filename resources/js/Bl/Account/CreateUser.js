import MESSAGE_STRINGS from "../../Constants/User/MessageStrings";
import URL from "../../Constants/User/Url";
import Toast from "../../Libraries/Toasts/Toast";
import setErrorMessages from "../../Libraries/ErrorMessages/setErrorMessages";
import GLOBAL_STRINGS from "../../Constants/Global/GlobalStrings";


class CreateUser
{
    constructor(oModel, oFormValidation, oUserFormElements)
    {
        this.oModel = oModel;
        this.oFormValidation = oFormValidation;
        this.oUserFormElements = oUserFormElements;
    }

    async register()
    {
        let oValidation = this.oFormValidation.checkErrors();
        if (oValidation.bResult === true) {
            let oResult = await this.oModel.save();
            this.showMessage(oResult);
            return;
        }
        this.displayErrorMessage(oValidation.bResult, oValidation.sMessage, '');
    }

    showMessage(oResult)
    {
        let sMessage = MESSAGE_STRINGS.REGISTRATION_SUCCESS;
        let oErrorMessage = new setErrorMessages(oResult[GLOBAL_STRINGS.RESULT], this.oUserFormElements);
        sMessage = oErrorMessage.setErrorMessages(sMessage, oResult[GLOBAL_STRINGS.B_RESULT]);
        this.displayErrorMessage(oResult[GLOBAL_STRINGS.B_RESULT], sMessage, URL.LOGIN_PAGE)
    }

    displayErrorMessage(bResult, sMessage, sUrl) {
        let oToast = new Toast(bResult, sMessage, sUrl);
        oToast.showToast();
    }

}
export default CreateUser;
