import MESSAGE_STRINGS from "../../Constants/Category/MessageStrings";
import Toast from "../../Libraries/Toasts/Toast";
import setErrorMessages from "../../Libraries/ErrorMessages/setErrorMessages";
import GLOBAL_STRINGS from "../../Constants/Global/GlobalStrings";
import ConfirmContinue from "../../Libraries/Confirmation/ConfirmContinue";
import FORM_OBJECTS from "../../Constants/FormObjects";

class AddCategory
{
    constructor(oModel, oFormValidation, oCategoryFormElements)
    {
        this.oModel = oModel;
        this.oFormValidation = oFormValidation;
        this.oCategoryFormElements = oCategoryFormElements;
    }

    validate()
    {
        let oValidation = this.oFormValidation.checkErrors();
        this.bResult = oValidation.bResult;
        this.sMessage = oValidation.sMessage;
    }

    async confirm()
    {
        if (this.bResult === true) {
            let oConfirmContinue = new ConfirmContinue(MESSAGE_STRINGS.CONFIRM_CONTINUE, MESSAGE_STRINGS.YES_LABEL);
            this.bResult = await oConfirmContinue.showConfirmation();
        }
    }

    async add()
    {
        this.validate();
        await this.confirm();
        if (this.bResult === true) {
            this.oResult = await this.oModel.save();
            this.showMessage(this.oResult);
            this.closeModal();
            return;
        }
        this.displayMessage(this.bResult, this.sMessage);
    }

    showMessage(oResult)
    {
        let sMessage = MESSAGE_STRINGS.ADD_SUCCESS;
        let oErrorMessage = new setErrorMessages(oResult[GLOBAL_STRINGS.RESULT], this.oCategoryFormElements);
        sMessage = oErrorMessage.setErrorMessages(sMessage, oResult[GLOBAL_STRINGS.B_RESULT]);
        this.displayMessage(oResult[GLOBAL_STRINGS.B_RESULT], sMessage, GLOBAL_STRINGS.ADMIN_URL)
    }

    displayMessage(bResult, sMessage = 'cancelled', sUrl) {
        let oToast = new Toast(bResult, sMessage, sUrl);
        oToast.showToast();
    }

    closeModal()
    {
        FORM_OBJECTS.CLOSE.click();
    }

}
export default AddCategory;
