import UserObjects from "../Constants/User/UserElements";
import ChangeBorder from "./ChangeBorder";

$(function() {
    let DOMEvents = {
        init: function () {
            this.registrationEvents();
        },

        registrationEvents() {
            let oFocusEvent = new ChangeBorder(UserObjects);
            oFocusEvent.resetBorderColor();
        },
    };
    DOMEvents.init();
});
