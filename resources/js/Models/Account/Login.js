import Loading from "../../Libraries/Curtain/Loading";
import URL from "../../Constants/Login/LoginUrl";
import ExtractField from "../ExtractField";

class Login
{
    constructor(oHttpRequest, oFields)
    {
        this.oFields = oFields;
        this.oLogin = oHttpRequest;
    }

    async authenticate()
    {
        let oExtractField = new ExtractField(this.oFields);
        let oFields = oExtractField.exec();

        let LoadingPopUp = new Loading('Authenticating...', 'foldingCube');
        return await this.oLogin.execute(oFields, URL.AUTHENTICATE, LoadingPopUp);
    }
}
export default Login
