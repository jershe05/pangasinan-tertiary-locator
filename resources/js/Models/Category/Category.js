import URL from "../../Constants/Category/Url";
import Loading from "../../Libraries/Curtain/Loading";
import ExtractField from "../ExtractField";

class Category
{
    constructor(oHttpRequest, oFields)
    {
        this.oCategoryRequest = oHttpRequest;
        this.oFields = oFields;
    }

   async save()
    {
        let oExtractField = new ExtractField(this.oFields);
        let oFields = oExtractField.exec();
        let LoadingPopUp = new Loading('Saving...', 'foldingCube');
        return await this.oCategoryRequest.execute(oFields, URL.SAVE_CATEGORY, LoadingPopUp);
    }

    async delete()
    {

    }

    async update()
    {

    }

    async uploadPicture()
    {

    }


}
export default Category;
