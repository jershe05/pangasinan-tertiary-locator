class HttpRequest
{
    execute(oParam, sUrl, oLoading)
    {
        oLoading.show();
        return window.axios.post(
            sUrl,
            {
                params: oParam,
            }).then(function(oResponse) {
                return oResponse.data;
            })
            .catch(function() {
                return 'error';
            })
            .finally(function() {
                oLoading.destroy();
            });
    }
}

export default HttpRequest;
