<?php

namespace App\Bl\Validation\ValidationObjects;

use Illuminate\Support\Facades\Hash;

class PasswordFormatField extends AbstractInputField
{
    public function formatText()
    {
         $this->sValue = bcrypt($this->sValue);
         return $this;
    }
}
