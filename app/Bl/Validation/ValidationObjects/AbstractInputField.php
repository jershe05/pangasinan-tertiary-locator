<?php

namespace App\Bl\Validation\ValidationObjects;

use App\Constants\AppStrings;
use App\Contracts\Bl\Validation\ValidationObjectContract;
use Illuminate\Support\Facades\Validator;

Abstract class AbstractInputField implements ValidationObjectContract
{
    protected $aMessage;
    protected $sField;
    protected $sValue;
    protected $sRules;

    public function __construct(string $sRules)
    {
        $this->sRules = $sRules;
    }

    public function validateField(string $sValue, string $sField) : ValidationObjectContract
    {
        $this->sField = $sField;
        $this->sValue = $sValue;
        $aParams = [
            $this->sField => $this->sValue
        ];
        $res =  Validator::make(
            $aParams,
            array($this->sField => $this->sRules)
        );
        $this->aMessage = $res->getMessageBag()->getMessages();
        return $this;
    }

    public function getResultAndErrorMessages() : array
    {
        if (!empty($this->aMessage[$this->sField])) {
            return [
                $this->sField => $this->sField,
                AppStrings::IS_VALID => false,
                AppStrings::RESULT => [
                    AppStrings::MESSAGES => $this->aMessage[$this->sField]
                ]
            ];
        }

        return [
            $this->sField        => $this->sField,
            AppStrings::IS_VALID => true,
            AppStrings::RESULT   => $this->sValue
        ];
    }
}
