<?php

namespace App\Bl\Authentication;

use App\Constants\AppStrings;
use Illuminate\Support\Facades\Auth;

class UserAuthentication
{
    private $aCredentials;
    public function __construct($aCredentials)
    {
        $this->aCredentials = $aCredentials;
    }

    public function authenticate()
    {
        if (Auth::attempt($this->aCredentials)) {
            return [AppStrings::B_RESULT => true, AppStrings::URL => AppStrings::LOGIN_ROUTE];
        }

        $aError = [
                AppStrings::EMAIL_FIELD => [
                AppStrings::MESSAGES => AppStrings::WRONG_LOGIN
            ],
            AppStrings::PASSWORD_FIELD => [],
        ];

        return [
            AppStrings::B_RESULT => false,
            AppStrings::RESULT  => $aError
        ];
    }
}
