<?php

namespace App\Constants;

class AppStrings
{
    //Database Model
    const USER_MODEL = 'UserModel';
    const PRODUCT_MODEL = 'ProductModel';
    const USER_TABLE = 'users';
    const CATEGORY_TABLE = 'category';

    //cache key
    const USER_CACHE = 'user';

    //Routes
    const USER_ROUTES = [
        'namespace' => 'App\Controllers\Registration',
        'path'      => 'app/Routes/UserRegistrationRoutes.php'
    ];

    const AUTHENTICATE = [
        'namespace' => 'App\Controllers\Category',
        'path'      => 'app/Routes/CategoryRoutes.php'
    ];

    const CATEGORY_ROUTES = [
        'namespace' => 'App\Controllers\Auth',
        'path'      => 'app/Routes/AccountRoutes.php'
    ];

    // user
    const EMAIL_FIELD = 'email';
    const FNAME_FIELD = 'fname';
    const LNAME_FIELD = 'lname';
    const PASSWORD_FIELD = 'password';
    const CONFIRM_PASSWORD_FIELD = 'confirm_password';

    // category

    const CATEGORY_NAME = 'name';
    const CATEGORY_DESCRIPTION = 'description';

    //
    const IS_VALID = 'isValid';
    const VALUE = 'value';
    const MESSAGES = 'messages';
    const B_RESULT = 'bResult';
    const RESULT = 'result';
    const PARAMS = 'params';
    const URL = 'url';

    // user field rules
    const EMAIL_RULES = 'required|email|unique:users,email';
    const EMAIL_LOGIN_RULES = 'required|email';
    const FNAME_RULES = 'required';
    const LNAME_RULES = 'nullable';
    const PASSWORD_RULES = 'required|min:8';

    // category field rules
    const CATEGORY_NAME_RULES = 'required';
    const CATEGORY_DESCRIPTION_RULES = 'nullable';

    //messages
    const  DID_NOT_MATCH = 'did not match';
    const WRONG_LOGIN = 'Wrong login Credentials!';

    //Url
    const LOGIN_ROUTE = '/';

    const ACCOUNT_ROUTES = [
        '/admin',
        '/pos'
    ];
    const AUTHENTICATE_ROUTE = '/authenticate';
    const FORGOT_PASSWORD = '/forgot-password';
    const REGISTER_ROUTE = '/register';
    const SAVE_REGISTRATION = '/registration/save';
    const LOG_OUT_ROUTE = '/logout';
    const SAVE_CATEGORY_ROUTE = '/category/save';

}
