<?php

namespace App\Http\Middleware;

use App\Contracts\Model\AccountContract;
use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    private $oAccountModel;
    public function __construct(AccountContract $oAccountModel)
    {
        $this->oAccountModel = $oAccountModel;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */

    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check() && $this->checkAdmin()) {
            return $next($request);
        }
       return App::abort(403);
    }
    private function checkAdmin()
    {
        return ($this->oAccountModel->getRoles(Auth::id()) === 0) ? true: false;
    }

}
