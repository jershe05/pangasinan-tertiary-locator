<?php

namespace App\Http\Middleware;

use App\Constants\AppStrings;
use App\Contracts\Model\AccountContract;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    private $oAccountModel;
    public function __construct(AccountContract $oAccountModel)
    {
        $this->oAccountModel = $oAccountModel;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */

    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return redirect($this->getUrl());
        }
        return $next($request);
    }

    public function getUrl()
    {
        $iRole = $this->oAccountModel->getRoles(Auth::id());
        return AppStrings::ACCOUNT_ROUTES[$iRole];
    }
}
