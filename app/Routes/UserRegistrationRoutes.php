<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Constants\AppStrings;

Route::get(AppStrings::REGISTER_ROUTE, function () {
    return view('account.index');
});

Route::post(AppStrings::SAVE_REGISTRATION, 'UserRegistrationController@store');


