<?php

namespace App\Controllers\Registration;

use App\Constants\AppStrings;
use App\Contracts\Bl\RegistrationContract;
use App\Contracts\Bl\Validation\ValidationContract;
use App\Controllers\Controller;

Abstract class RegistrationControllerAbstract extends Controller
{
    protected $oBlRegistration;
    protected $oBlValidation;

    public function __construct(RegistrationContract $oBlRegistration, ValidationContract $oBlValidation) {
        $this->oBlRegistration = $oBlRegistration;
        $this->oBlValidation = $oBlValidation;
    }

    protected function save(array $aRequestDetails, array $oFields, array $aFieldsToMatch): array
    {
        $aResultAndMessages = $this->validateFields($oFields, $aRequestDetails, $aFieldsToMatch);
        if ($aResultAndMessages[AppStrings::B_RESULT] === false) {
            return $aResultAndMessages;
        }
        return $this->oBlRegistration->register($aResultAndMessages[AppStrings::RESULT]);
    }

    private function validateFields(array $oFields, array $aUserDetails, array $aFieldsToMatch) : array
    {
        return $this->oBlValidation
            ->registerFields($oFields)
            ->loopThroughObjectsAndExecuteValidation($aUserDetails, $aFieldsToMatch)
            ->filterResult()
            ->getResult();
    }
}
