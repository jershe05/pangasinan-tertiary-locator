<?php

namespace App\Controllers\Category;

use App\Bl\Validation\ValidationObjects\DefaultField;
use App\Constants\AppStrings;
use App\Contracts\Bl\RegistrationContract;
use App\Contracts\Bl\Validation\ValidationContract;
use App\Contracts\Controllers\RegisterControllerContract;
use App\Controllers\Registration\RegistrationControllerAbstract;
use Illuminate\Http\Request;

class CategoryRegistrationController  extends RegistrationControllerAbstract implements RegisterControllerContract
{
    public function __construct(RegistrationContract $oBlRegistration, ValidationContract $oBlValidation)
    {
        parent::__construct($oBlRegistration, $oBlValidation);
    }

    public function instantiateObjectFields(): array
    {
        return [
            AppStrings::CATEGORY_NAME        => new DefaultField(AppStrings::CATEGORY_NAME_RULES),
            AppStrings::CATEGORY_DESCRIPTION => new DefaultField(AppStrings::CATEGORY_DESCRIPTION_RULES)
        ];
    }

    public function store(Request $aCategoryDetails) : array
    {
        $aCategoryDetails = $aCategoryDetails->all()[AppStrings::PARAMS];
        $oFields = $this->instantiateObjectFields();
        $aFieldsToMatch = $this->setFieldsToMatch();
        return $this->save($aCategoryDetails, $oFields, $aFieldsToMatch);
    }

    public function setFieldsToMatch() : array
    {
        return [];
    }
}
