<?php

namespace App\Providers;

use App\Bl\FormatText\FormatTextNone;
use App\Bl\FormatText\FormatTextTitle;
use App\Bl\Registration\CategoryRegistration;
use App\Bl\Registration\UserRegistration;
use App\Bl\Validation\Validation;
use App\Bl\Validation\ValidationObjects\TitleFormatField;
use App\Contracts\Bl\FormatTextContract;
use App\Contracts\Bl\RegistrationContract;
use App\Contracts\Bl\Validation\ValidationContract;
use App\Controllers\Category\CategoryRegistrationController;
use App\Controllers\Registration\UserRegistrationController;
use Illuminate\Support\ServiceProvider;

class RegistrationServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(UserRegistrationController::class)
            ->needs(RegistrationContract::class)
            ->give(UserRegistration::class);

        $this->app->when(CategoryRegistrationController::class)
            ->needs(RegistrationContract::class)
            ->give(CategoryRegistration::class);

        $this->app->when(UserRegistrationController::class)
            ->needs(RegistrationContract::class)
            ->give(UserRegistration::class);

            //->give(function($app){
            //    $aFieldObjects = [
            //          AppStrings::EMAIL_FIELD => new Email()
            //    ];
            //    new Validation($aFieldObjects);
            //});

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
