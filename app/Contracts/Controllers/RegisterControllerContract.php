<?php
namespace App\Contracts\Controllers;

use Illuminate\Http\Request;

Interface RegisterControllerContract
{
    public function instantiateObjectFields() : array;
    public function store(Request $aRequestDetails) : array;
    public function setFieldsToMatch() : array;
}
